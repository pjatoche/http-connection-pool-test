package org.pjatoche.test;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface LocalService {

    @GET("consulta-external-app")
    Call<Consulta> consultar(@Query("query") String consulta);
}
