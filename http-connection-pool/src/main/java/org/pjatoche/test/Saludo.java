package org.pjatoche.test;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Saludo{



    private final long id;
    private final String contenido;


    public Saludo(long id, String contenido){
        this.id = id;
        this.contenido = contenido;
    }

}