package org.pjatoche.test;


import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import okhttp3.ConnectionPool;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@SpringBootApplication
public class HttpConnectionPoolApplication {

    Logger LOGGER = LogManager.getLogger(HttpConnectionPoolApplication.class);

    @Value("${http-connection-pool.max-idle-connections}")
    private int maxIdleConnections;
    @Value("${http-connection-pool.keep-alive-duration}")
    private long keepAliveDuration;
    @Value("${ok-http-client.connect-time-out}")
    private long connectTimeOut;
    @Value("${ok-http-client.read-time-out}")
    private long readTimeOut;
    @Value("${request.base-url}")
    private String baseUrl;

    private static final TimeUnit TIME_UNIT = TimeUnit.SECONDS;

    @Bean
    public ConnectionPool createConnectionPool() {
        ConnectionPool f = null;
        return new ConnectionPool(maxIdleConnections, keepAliveDuration, TIME_UNIT);
    }

    @Bean(name="applicationInterceptor")
    public Interceptor createApplicationInterceptor(){
        Interceptor interceptor = new Interceptor(){
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Response response = chain.proceed(chain.request());
                LOGGER.info(chain+"----->ApplicationInterceptor");
                return response;
            }
        };
        return interceptor;
    }

    @Bean(name="networkInterceptor")
    public HttpLoggingInterceptor createNetworkInterceptor(){
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        //LOGGER.info("----->NetworkInterceptor");
        return httpLoggingInterceptor;
    }

    @Bean
    public OkHttpClient createOkHttpClient(@Autowired ConnectionPool connectionPool,
                                           @Autowired @Qualifier("networkInterceptor") HttpLoggingInterceptor networkInterceptor,
                                           @Autowired @Qualifier("applicationInterceptor") Interceptor applicationInterceptor) {


        OkHttpClient client = new OkHttpClient.Builder()
                //.addNetworkInterceptor(networkInterceptor)
                //.addInterceptor(applicationInterceptor)
                .connectTimeout(connectTimeOut, TIME_UNIT)
                .readTimeout(readTimeOut, TIME_UNIT)
                .connectionPool(connectionPool)
                .build();
        return client;
    }

    @Bean
    public Retrofit createRetrofit(@Autowired OkHttpClient okHttpClient) {

        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }


    public static void main(String[] args) {
        SpringApplication.run(HttpConnectionPoolApplication.class, args);
    }
}