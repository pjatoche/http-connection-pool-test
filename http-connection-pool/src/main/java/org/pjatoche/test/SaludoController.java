package org.pjatoche.test;


import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Calendar;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

@RestController
public class SaludoController {

    Logger LOGGER = LogManager.getLogger(SaludoController.class);

    private static final String TEMPLATE_RETROFIT = "RETROFIT-CALL-%d";
    private static final String TEMPLATE_RETROFIT_ASYNC = "RETROFIT-CALL-ASYNC-%d";
    private final AtomicLong counter = new AtomicLong();

    @Autowired
    private Retrofit retrofit;
    @Autowired
    private RetrofitConnectionPool retrofitConnectionPool;

    @RequestMapping("/greeting-retrofit")
    public Consulta greetingUsingRetrofit() {

        int id = (int) counter.incrementAndGet();
        String texto = String.format(TEMPLATE_RETROFIT, id);

        LOGGER.info(String.format("Yape Rest begin\t:\t%s\t%s", texto, Calendar.getInstance().getTime()));

        LocalService localService = retrofit.create(LocalService.class);
        Consulta consulta = null;
        try {
            consulta = localService.consultar(texto).execute().body();
        } catch (SocketTimeoutException e) {
            LOGGER.error("Your customized message" + e.getMessage());
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
        LOGGER.info(String.format("Yape Rest end\t\t:\t%s\t%s", texto, Calendar.getInstance().getTime()));
        return consulta;
    }


    @RequestMapping("/greeting-retrofit-pool")
    public Consulta greetingUsingRetrofitPool() {

        int id = (int) counter.incrementAndGet();
        String texto = String.format(TEMPLATE_RETROFIT, id);

        LOGGER.info(String.format("Yape Rest begin\t:\t%s\t%s", texto, Calendar.getInstance().getTime()));


        Consulta consulta = null;
        RetrofitRequest retrofitRequest = new RetrofitRequest() {
            @Override
            public Consulta call() {
                LocalService localService = retrofit.create(LocalService.class);
                Consulta resultado = null;
                try {
                    resultado = localService.consultar(texto).execute().body();
                } catch (SocketTimeoutException e) {
                    LOGGER.error("Your customized message" + e.getMessage());
                } catch (IOException e) {
                    LOGGER.error(e.getMessage(), e);
                }
                return resultado;
            }

            @Override
            public String getReference() {
                return texto;
            }
        };

        try {
            consulta = (Consulta) retrofitConnectionPool.request(retrofitRequest);
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage());
        }

        LOGGER.info(String.format("Yape Rest end\t\t:\t%s\t%s", texto, Calendar.getInstance().getTime()));
        return consulta;
    }

    @RequestMapping("/greeting-retrofit-async")
    public void greetingUsingRetrofitAsync() {

        int id = (int) counter.incrementAndGet();
        String texto = String.format(TEMPLATE_RETROFIT_ASYNC, id);

        LOGGER.info(String.format("Yape Rest Async begin\t:\t%s\t%s", texto, Calendar.getInstance().getTime()));

        LocalService localService = retrofit.create(LocalService.class);

        localService.consultar(texto).enqueue(new Callback<Consulta>() {
            @Override
            public void onResponse(Call<Consulta> call, Response<Consulta> response) {
                LOGGER.info(response.message());
                LOGGER.info(response.body().toString());
            }

            @Override
            public void onFailure(Call<Consulta> call, Throwable throwable) {
                LOGGER.error(throwable.getMessage());
            }
        });

        LOGGER.info(String.format("Yape Rest Async end\t\t:\t%s\t%s", texto, Calendar.getInstance().getTime()));
    }
}