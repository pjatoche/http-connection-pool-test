package org.pjatoche.test;

import org.apache.http.client.HttpClient;

import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.stereotype.Service;

@Service
public class PoolingHttp {


    HttpClient httpClient;
    private final int SOCKET_TIMEOUT_MS = 10;
    private final int CONNECTION_TIMEOUT_MS = 20;
    private final int CONNECTION_REQ_TIMEOUT_MS = 30;

    private final int MAX_SIMULTANEOUS_CONNECTIONS = 5;
    private final int MAX_PER_ROUTE = 5;

    public PoolingHttp(){

        RequestConfig requestConfig = RequestConfig.custom()
                .setSocketTimeout(SOCKET_TIMEOUT_MS)
                .setConnectTimeout(CONNECTION_TIMEOUT_MS)
                .setConnectionRequestTimeout(CONNECTION_REQ_TIMEOUT_MS)
                .setCookieSpec(CookieSpecs.STANDARD)
                .build();

        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();

        connectionManager.setMaxTotal(MAX_SIMULTANEOUS_CONNECTIONS);
        connectionManager.setDefaultMaxPerRoute(MAX_PER_ROUTE);

        httpClient = HttpClientBuilder
                .create()
                .setDefaultRequestConfig(requestConfig)
                .setConnectionManager(connectionManager)
                .build();

    }

    public HttpClient getHttpClient() {
        return httpClient;
    }
}
