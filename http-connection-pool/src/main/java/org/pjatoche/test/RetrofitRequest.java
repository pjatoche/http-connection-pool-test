package org.pjatoche.test;

public interface RetrofitRequest <T>{
    T call();

    String getReference();
}
