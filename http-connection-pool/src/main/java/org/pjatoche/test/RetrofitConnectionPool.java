package org.pjatoche.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import okhttp3.ConnectionPool;

@Service
public class RetrofitConnectionPool {

    Logger LOGGER = LogManager.getLogger(RetrofitConnectionPool.class);

    @Autowired
    ConnectionPool connectionPool;

    @Value("${retrofit-connection-pool.max-connections}")
    int maxConnections;

    @Value("${retrofit-connection-pool.capacity-timeout}")
    long capacityTimeout;

    @Value("${retrofit-connection-pool.ask-capacity-delay}")
    long askCapacityDelay;

    public Object request(RetrofitRequest retrofitRequest) throws InterruptedException{

        long timeout = 0;
        LOGGER.info("\tReceiving a request --->" + retrofitRequest.getReference());
        while (connectionPool.connectionCount() > maxConnections) {
            if (timeout > capacityTimeout) {
                String message = String.format("Capacity timeout exception, %s, request in buffer %d " +
                        "exceeds setup capacityTimeout:%d", retrofitRequest.getReference(), timeout, capacityTimeout);
                throw new InterruptedException(message);
            }
            LOGGER.info("\t\tWaiting for a connection--->" + retrofitRequest.getReference());
            Thread.sleep(askCapacityDelay);
            timeout += askCapacityDelay;
        }
        LOGGER.info("\tCalling--->" + retrofitRequest.getReference());
        Object response = retrofitRequest.call();
        return response;
    }
}
