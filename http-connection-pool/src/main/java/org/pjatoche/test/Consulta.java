package org.pjatoche.test;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class Consulta {
    private final long id;
    private final String query;
    private final int delay;
}