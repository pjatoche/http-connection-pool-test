package org.pjatoche.test;

import java.util.Calendar;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.lang3.RandomUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConsultaController {

    Logger LOGGER = LogManager.getLogger(ConsultaController.class);
    private static final String TEMPLATE = "Query: %s";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/consulta-external-app")
    public Consulta consulta(@RequestParam(value="query", defaultValue="consulta") String name) {

        long id = counter.incrementAndGet();
        String query = String.format(TEMPLATE, name);
        int delay = getDelay();
        Consulta consulta = Consulta.builder()
                .id(id)
                .query(query)
                .delay(delay)
                .build();
        try {
            LOGGER.info(String.format("Begin\t\t%d\t%s\t%s", consulta.getId(), consulta.getQuery(), Calendar.getInstance().getTime()));
            Thread.sleep(delay);
            LOGGER.info(String.format("End\t\t%d\t%s\t%s\t\t%d%n", consulta.getId(), consulta.getQuery(), Calendar.getInstance().getTime(), consulta.getDelay()));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return consulta;
    }

    private int getDelay(){

        int MIN = 4;
        int MAX = 10;
        int MILLISECONDS = 1000;
        return RandomUtils.nextInt(MIN, MAX)*MILLISECONDS;

    }
}
