package org.pjatoche.test;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Consulta {
    private final long id;
    private final String query;
    private final int delay;
}